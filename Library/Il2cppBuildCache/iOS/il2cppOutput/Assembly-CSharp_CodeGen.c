﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Ball::OnMouseDown()
extern void Ball_OnMouseDown_mBC831AE8F66192AF6C8B739D7C8D053D98827D71 (void);
// 0x00000002 System.Void Ball::OnMouseDrag()
extern void Ball_OnMouseDrag_m7A08050A67D0E8B51C9C61914408BB8BCAD4706B (void);
// 0x00000003 System.Void Ball::Update()
extern void Ball_Update_mE1E2A0FEFCA0034896BB36FD9C75718E4D14E3B2 (void);
// 0x00000004 System.Void Ball::Die()
extern void Ball_Die_m2A287E9B40A929F11ED97305DF3D40D7883005F4 (void);
// 0x00000005 System.Void Ball::OnCollisionEnter(UnityEngine.Collision)
extern void Ball_OnCollisionEnter_m2CBD6B8B504E7715B679BAEA26C98041F34FB514 (void);
// 0x00000006 System.Void Ball::.ctor()
extern void Ball__ctor_mDFF93D8FA13BD70DF4F2FA2B9B402E3B10BD6EDC (void);
// 0x00000007 System.Void Ball::.cctor()
extern void Ball__cctor_m82987A0E2696107984545ED00D25D6ED7784330F (void);
// 0x00000008 System.Void CollectCoin::OnTriggerEnter(UnityEngine.Collider)
extern void CollectCoin_OnTriggerEnter_m5738414530342A95A00FF4DE759283FBF2375703 (void);
// 0x00000009 System.Void CollectCoin::.ctor()
extern void CollectCoin__ctor_mECFCFA12502994A2E3C13AC7E09C3B365B439C89 (void);
// 0x0000000A System.Void CollectControl::Update()
extern void CollectControl_Update_m11243810576FA74D685BE1A6AD2730D3851674EB (void);
// 0x0000000B System.Void CollectControl::.ctor()
extern void CollectControl__ctor_m53C02E75F37012020ED383F3FCC07C8AE174B34F (void);
// 0x0000000C System.Void GameOverScreen::Setup(System.Int32)
extern void GameOverScreen_Setup_m556774F243CE153B56DF185F36A0AF5624FD66FC (void);
// 0x0000000D System.Void GameOverScreen::RestartButton()
extern void GameOverScreen_RestartButton_m5AA1EEADBACE674076413DF326CBED43654A98F1 (void);
// 0x0000000E System.Void GameOverScreen::.ctor()
extern void GameOverScreen__ctor_mC5150146F14E85405429254671C40F23C5BD9F20 (void);
// 0x0000000F System.Void RandomSection::Update()
extern void RandomSection_Update_mE26527A4BF4032BE171DF2BF45951A3AFB7AEE61 (void);
// 0x00000010 System.Collections.IEnumerator RandomSection::GenerateSection()
extern void RandomSection_GenerateSection_mF82B710FACD1E99ADAC77547314503BD3317055F (void);
// 0x00000011 System.Void RandomSection::.ctor()
extern void RandomSection__ctor_m6C8463FC2B54916D1D89A6F660F5673807A4F954 (void);
// 0x00000012 System.Void RandomSection/<GenerateSection>d__5::.ctor(System.Int32)
extern void U3CGenerateSectionU3Ed__5__ctor_mE6D44F5A56F83B88C2C31A3CA8388BE715D1CA82 (void);
// 0x00000013 System.Void RandomSection/<GenerateSection>d__5::System.IDisposable.Dispose()
extern void U3CGenerateSectionU3Ed__5_System_IDisposable_Dispose_mFA24F81654BCBD0C4B9708015EB5A89A80C19924 (void);
// 0x00000014 System.Boolean RandomSection/<GenerateSection>d__5::MoveNext()
extern void U3CGenerateSectionU3Ed__5_MoveNext_m1C8E8EE908A0E4336E5724C1BA480EFDFE6263C4 (void);
// 0x00000015 System.Object RandomSection/<GenerateSection>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateSectionU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0447F30303636ABC2146BD060ABEE633D0ACD0F9 (void);
// 0x00000016 System.Void RandomSection/<GenerateSection>d__5::System.Collections.IEnumerator.Reset()
extern void U3CGenerateSectionU3Ed__5_System_Collections_IEnumerator_Reset_m51637FE9C0210FA9416A02EDCFAB6DEA7EBB0526 (void);
// 0x00000017 System.Object RandomSection/<GenerateSection>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateSectionU3Ed__5_System_Collections_IEnumerator_get_Current_m425D8B4D9F62988D7E085809E3CB0046B22DA235 (void);
static Il2CppMethodPointer s_methodPointers[23] = 
{
	Ball_OnMouseDown_mBC831AE8F66192AF6C8B739D7C8D053D98827D71,
	Ball_OnMouseDrag_m7A08050A67D0E8B51C9C61914408BB8BCAD4706B,
	Ball_Update_mE1E2A0FEFCA0034896BB36FD9C75718E4D14E3B2,
	Ball_Die_m2A287E9B40A929F11ED97305DF3D40D7883005F4,
	Ball_OnCollisionEnter_m2CBD6B8B504E7715B679BAEA26C98041F34FB514,
	Ball__ctor_mDFF93D8FA13BD70DF4F2FA2B9B402E3B10BD6EDC,
	Ball__cctor_m82987A0E2696107984545ED00D25D6ED7784330F,
	CollectCoin_OnTriggerEnter_m5738414530342A95A00FF4DE759283FBF2375703,
	CollectCoin__ctor_mECFCFA12502994A2E3C13AC7E09C3B365B439C89,
	CollectControl_Update_m11243810576FA74D685BE1A6AD2730D3851674EB,
	CollectControl__ctor_m53C02E75F37012020ED383F3FCC07C8AE174B34F,
	GameOverScreen_Setup_m556774F243CE153B56DF185F36A0AF5624FD66FC,
	GameOverScreen_RestartButton_m5AA1EEADBACE674076413DF326CBED43654A98F1,
	GameOverScreen__ctor_mC5150146F14E85405429254671C40F23C5BD9F20,
	RandomSection_Update_mE26527A4BF4032BE171DF2BF45951A3AFB7AEE61,
	RandomSection_GenerateSection_mF82B710FACD1E99ADAC77547314503BD3317055F,
	RandomSection__ctor_m6C8463FC2B54916D1D89A6F660F5673807A4F954,
	U3CGenerateSectionU3Ed__5__ctor_mE6D44F5A56F83B88C2C31A3CA8388BE715D1CA82,
	U3CGenerateSectionU3Ed__5_System_IDisposable_Dispose_mFA24F81654BCBD0C4B9708015EB5A89A80C19924,
	U3CGenerateSectionU3Ed__5_MoveNext_m1C8E8EE908A0E4336E5724C1BA480EFDFE6263C4,
	U3CGenerateSectionU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0447F30303636ABC2146BD060ABEE633D0ACD0F9,
	U3CGenerateSectionU3Ed__5_System_Collections_IEnumerator_Reset_m51637FE9C0210FA9416A02EDCFAB6DEA7EBB0526,
	U3CGenerateSectionU3Ed__5_System_Collections_IEnumerator_get_Current_m425D8B4D9F62988D7E085809E3CB0046B22DA235,
};
static const int32_t s_InvokerIndices[23] = 
{
	3254,
	3254,
	3254,
	3254,
	2646,
	3254,
	5021,
	2646,
	3254,
	3254,
	3254,
	2629,
	3254,
	3254,
	3254,
	3172,
	3254,
	2629,
	3254,
	3114,
	3172,
	3254,
	3172,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	23,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
